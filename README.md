## Rust Telnet Client library

### Very rudimentary but production tested telnet client for executing simple commands

### Remarks:
  - tested with ZTE C320 OLT
  - if you are executing nested commands (conf t/interface/etc) always remember to add "end\n" at the command end.
    This will cd to top level so prompt will be matching and you will not endup with hanging command
    



