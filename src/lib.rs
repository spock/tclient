use anyhow::Result;
use ping::rawsock;
use std::net::IpAddr;
use std::{
    str::{self, FromStr},
    time::Duration,
};
use telnet::{Event, Telnet};
use thiserror::Error;
use tracing::{debug, warn};

#[derive(Error, Debug)]
pub enum TclientError {
    #[error("Invalid Command Parameter")]
    InvalidCommandParameter(String),
    #[error("Executing command error")]
    CommandExecuteError(String),
    #[error("Connecting error")]
    ConnectionError(String),
}
pub fn send_and_receive(t: &mut Telnet, data: &str, expect: &str) -> Result<String> {
    debug!("Sending text:{}", data);
    let wr = t.write(format!("{}\n", data).as_bytes());
    match wr {
        Ok(_w) => {}

        Err(e) => {
            warn!("Error sending data: {}", e);
            // std::process::exit(0);
            warn!("Eror sending command: {}", e);
        }
    };
    // not best performant by + string but this is not a issue with that scale
    let mut notdone = true;
    let mut result = String::new();
    while notdone {
        debug!("Waiting for result ({})..", &expect);
        let event = t.read();
        if let Err(e) = event {
            return Err(TclientError::ConnectionError(e.to_string()).into());
        };
        let event = event.unwrap();
        if let Event::Data(d) = event {
            let tinput = str::from_utf8(&d)?;
            if !tinput.ends_with(expect) {
                debug!(
                    "Partial recieve:\n-------------------------\n{}\n-------------------",
                    &tinput
                );
                result.push_str(tinput);
            } else {
                debug!("Got expected result({}:{})", &expect, &tinput);
                result.push_str(tinput);
                notdone = false;
            }
        }
        //     let tinput = str::from_utf8(&d).unwrap();
        //     println!("send_and_recieve() {}", tinput);
    }
    let result = result.replace(expect, "");
    // println!("{}", result);
    // }
    Ok(result)
}
pub fn telnet_exec(
    host: &str,
    port: u16,
    login: &str,
    password: &str,
    prompt: &str,
    command: &str,
    check_alive: Option<Duration>,
) -> Result<String> {
    debug!("Connecting to {}", host);
    debug!("Checking if host is alive.");
    if let Some(timeout) = check_alive {
        let _ping = rawsock::ping(
            IpAddr::from_str(host).unwrap(),
            Some(timeout),
            Some(32),
            None,
            None,
            None,
        )?;
        debug!("Host is alive");
    }
    let mut connection = Telnet::connect((host, port), 2048).unwrap();
    // let mut loopcnt = 1;

    let result = loop {
        let event = connection.read().expect("Read Error");
        //let mut result = String::new();
        match event {
            telnet::Event::Data(d) => {
                let tinput = str::from_utf8(&d).unwrap();
                debug!("telnet_exec loop {}", tinput);
                let _t = send_and_receive(&mut connection, login, "Password:")?;
                let _t = send_and_receive(&mut connection, password, prompt)?;
                let _t = send_and_receive(&mut connection, "terminal length 0", prompt)?;
                let _t = send_and_receive(&mut connection, command, prompt)?;
                //debug!("{}", result);
                // loopcnt += 1;
                //std::process::exit(0);
                if _t.contains("%Error") {
                    return Err(TclientError::CommandExecuteError(_t).into());
                }
                if _t.contains("%Code") {
                    return Err(TclientError::InvalidCommandParameter(_t).into());
                }
                debug!("{}", _t);

                break _t;
            }
            telnet::Event::Negotiation(..) => {}
            _ => {
                debug!("Telnet Event: {:?}", event);
            }
        }
    };
    Ok(result)
}
