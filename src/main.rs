use anyhow::Result;
use clap::Parser;
use std::str;
use tclient::telnet_exec;
use tracing::info;
use tracing_subscriber::{filter::LevelFilter, EnvFilter};

// Basic example how you use this library

const ADD_ONT: &str = "conf t
interface {}
onu {} type {} sn {}
end
";

#[tokio::main]
async fn main() -> Result<()> {
    let ef = EnvFilter::builder()
        .with_default_directive(LevelFilter::INFO.into())
        .with_env_var("RUST_LOG")
        .from_env_lossy();
    tracing_subscriber::fmt::fmt().with_env_filter(ef).init();
    let cli = Cli::parse();
    info!("Connecting to {} (timeout: {} sec)", &cli.host, &cli.timeout);
    println!("command:{}", &cli.command[0]);
    if &cli.command[0] == "add_onu" {
        let _ = (&cli.host, 23, &cli.username, &cli.password, "C320#", ADD_ONT);
    } else {
        let _ = telnet_exec(
            &cli.host,
            23,
            &cli.username,
            &cli.password,
            "C320#",
            &cli.command[0],
            None,
        );
    };
    // connect
    Ok(())
}

#[derive(clap::Parser)]
#[clap(trailing_var_arg = true)]
pub struct Cli {
    #[clap(index = 1)]
    host: String,

    // #[clap(long, short, default_value_t = 22)]
    // port: u16,
    #[clap(long, short)]
    username: String,
    #[clap(long, short)]
    password: String,

    // #[clap(long, short = 'k')]
    // private_key: PathBuf,
    #[clap(long, short, default_value_t = 30)]
    timeout: u32,

    #[clap(index = 2, required = true)]
    command: Vec<String>,
}
